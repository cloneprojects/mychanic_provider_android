package com.app.mymechanicexpert.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.mymechanicexpert.FCM.LocationService;
import com.app.mymechanicexpert.R;
import com.app.mymechanicexpert.Volley.ApiCall;
import com.app.mymechanicexpert.Volley.VolleyCallback;
import com.app.mymechanicexpert.activities.AboutUs;
import com.app.mymechanicexpert.activities.ChangePasswordActivity;
import com.app.mymechanicexpert.activities.EditAddress;
import com.app.mymechanicexpert.activities.EditProfileActivity;
import com.app.mymechanicexpert.activities.FaqActivity;
import com.app.mymechanicexpert.activities.MainActivity;
import com.app.mymechanicexpert.activities.SignInActivity;
import com.app.mymechanicexpert.activities.TermsAndConditions;
import com.app.mymechanicexpert.activities.ViewCategory;
import com.app.mymechanicexpert.activities.ViewSchedule;
import com.app.mymechanicexpert.adapters.AccountsAdapter;
import com.app.mymechanicexpert.helpers.AppSettings;
import com.app.mymechanicexpert.helpers.UrlHelper;
import com.app.mymechanicexpert.helpers.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccountsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AccountsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    LinearLayout logOut, viewCategory, viewSchedule, viewEditAddress;
    TextView providerName;
    TextView providerMobile;
    ImageView profilePicture;
    LinearLayout changeProfile;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    JSONObject profileDetails = new JSONObject();

    private OnFragmentInteractionListener mListener;
    private LinearLayout changePassword;
    private String TAG = AccountsFragment.class.getSimpleName();

    private List<Integer> allColors = new ArrayList<>();
    private ImageView changeThemIcon;
    private RecyclerView changeThemeAdapter;
    private boolean themeOpen;
    MainActivity activity;

    public AccountsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AccountsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountsFragment newInstance(String param1, String param2) {
        AccountsFragment fragment = new AccountsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        activity = (MainActivity) getContext();
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_account, container, false);
        logOut = (LinearLayout) view.findViewById(R.id.logOut);
        providerName = (TextView) view.findViewById(R.id.providerName);
        providerMobile = (TextView) view.findViewById(R.id.providerMobile);
        viewCategory = (LinearLayout) view.findViewById(R.id.viewCategory);
        viewSchedule = (LinearLayout) view.findViewById(R.id.viewSchedule);
        viewEditAddress = (LinearLayout) view.findViewById(R.id.viewEditAddress);
        profilePicture = (ImageView) view.findViewById(R.id.profilePic);
        changeProfile = (LinearLayout) view.findViewById(R.id.changeProfile);
        changePassword = (LinearLayout) view.findViewById(R.id.changePassword);

        LinearLayout changeTheme = view.findViewById(R.id.changeTheme);
        changeThemIcon = view.findViewById(R.id.changeThemeIcon);
        changeThemeAdapter = view.findViewById(R.id.changeThemeAdapter);


        ImageView account_profile = view.findViewById(R.id.account_profile);
        ImageView account_password = view.findViewById(R.id.account_password);
        ImageView account_address = view.findViewById(R.id.account_address);
        ImageView account_services = view.findViewById(R.id.account_services);
        ImageView account_schedule = view.findViewById(R.id.account_schedule);
        ImageView account_about = view.findViewById(R.id.account_about);
        ImageView account_theme = view.findViewById(R.id.account_theme);
        ImageView account_logout = view.findViewById(R.id.account_logout);

        Utils.setIconColour(activity, account_profile);
        Utils.setIconColour(activity, account_password);
        Utils.setIconColour(activity, account_address);
        Utils.setIconColour(activity, account_services);
        Utils.setIconColour(activity, account_schedule);
        Utils.setIconColour(activity, account_about);
        Utils.setIconColour(activity, account_theme);
        Utils.setIconColour(activity, account_logout);

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppSettings appSettings = new AppSettings(getActivity());
                appSettings.setIsLogged("false");
                try {
                    Intent stopintent = new Intent(getActivity(), LocationService.class);
                    getActivity().stopService(stopintent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(getActivity(), SignInActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);
            }
        });

        changeProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("provider_details", profileDetails.toString());
                startActivity(intent);
            }
        });
        viewEditAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditAddress.class);
                intent.putExtra("provider_details", profileDetails.toString());
                startActivity(intent);
            }
        });
        initGeneral(view);


        getValues();

        viewCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ViewCategory.class);
                startActivity(intent);
            }
        });
        viewSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ViewSchedule.class);
                startActivity(intent);
            }
        });

        themeOpen = false;
        allColors = Utils.getAllMaterialColors(activity);

        int numberOfColumns = 6;
        changeThemeAdapter.setLayoutManager(new GridLayoutManager(activity, numberOfColumns));
        AccountsAdapter adapter = new AccountsAdapter(activity, allColors);
        changeThemeAdapter.setAdapter(adapter);
        changeTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!themeOpen) {
                    expand(changeThemeAdapter);
                    themeOpen = true;
                } else {
                    collapse(changeThemeAdapter);
                    themeOpen = false;
                }
            }
        });
        return view;
    }

    public void expand(final View v) {
        changeThemIcon.setRotation(90);
        //changeThemeAdapter.setVisibility(View.VISIBLE);
        v.measure(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? RecyclerView.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapse(final View v) {
        changeThemIcon.setRotation(0);
        //changeThemeAdapter.setVisibility(View.GONE);
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    private void initGeneral(View view) {

        final LinearLayout faq, aboutUs, termsAndCondition;
        faq = view.findViewById(R.id.faq);
        aboutUs = view.findViewById(R.id.aboutUs);
        termsAndCondition = view.findViewById(R.id.termsAndCondition);


        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FaqActivity.class);
                startActivity(intent);
            }
        });

        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AboutUs.class);
                startActivity(intent);
            }
        });

        termsAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TermsAndConditions.class);
                startActivity(intent);
            }
        });
    }


    private void getValues() {
        ApiCall.getMethodHeaders(getActivity(), UrlHelper.VIEW_PROFILE, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d(TAG, "onSuccess: " + response);
                JSONObject userDetails = new JSONObject();
                userDetails = response.optJSONObject("provider_details");
                profileDetails = response.optJSONObject("provider_details");
                String fullname = userDetails.optString("first_name") + " " + userDetails.optString("last_name");
                String userMobile = userDetails.optString("mobile");
                providerName.setText(fullname);
                if (userMobile.length() != 0 && !userMobile.equalsIgnoreCase("null")) {
                    providerMobile.setText(userMobile);

                } else {
                    providerMobile.setText("");
                }
                Glide.with(getActivity())
                        .load(userDetails.optString("image"))
                        .placeholder(Utils.getProfilePicture(activity))
                        .dontAnimate().into(profilePicture);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
