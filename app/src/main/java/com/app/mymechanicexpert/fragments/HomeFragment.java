package com.app.mymechanicexpert.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.app.mymechanicexpert.Events.Status;
import com.app.mymechanicexpert.R;
import com.app.mymechanicexpert.Volley.ApiCall;
import com.app.mymechanicexpert.Volley.VolleyCallback;
import com.app.mymechanicexpert.adapters.BookingsAdapter;
import com.app.mymechanicexpert.helpers.UrlHelper;
import com.app.mymechanicexpert.helpers.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static RecyclerView bookingsList;
    public static String TAG = HomeFragment.class.getSimpleName();
    public static boolean isCommentVisible = false;
    public static SwipeRefreshLayout swiperefresh;
    public static RelativeLayout empty_layout;
    private static Dialog dialog;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BookingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static void getPendingRequests(final Context context) {
        ApiCall.getMethodHeaders(context, UrlHelper.HOME_DASHBOARD, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.log(TAG, ": responseValues:" + response);
                JSONArray pending = new JSONArray();
                pending = response.optJSONArray("Pending");
                if (swiperefresh.isRefreshing()) {
                    swiperefresh.setRefreshing(false);
                }
                if (pending.length() > 0) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = pending.optJSONObject(0);
                    showRequestDialog(jsonObject, context);
                }
                JSONArray accepted = new JSONArray();

                accepted = response.optJSONArray("Accepted");
                if (accepted.length() > 0) {
                    swiperefresh.setVisibility(View.VISIBLE);
                    empty_layout.setVisibility(View.GONE);
                    bookingsList.setVisibility(View.VISIBLE);
                    bookingsList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                    BookingsAdapter adapter = new BookingsAdapter(context, accepted);
                    bookingsList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    swiperefresh.setVisibility(View.VISIBLE);

                    empty_layout.setVisibility(View.VISIBLE);
                    bookingsList.setVisibility(View.GONE);
                }

                for (int i = 0; i < accepted.length(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject = accepted.optJSONObject(i);

                    if (jsonObject.optString("status").equalsIgnoreCase("Reviewpending")) {
                        if (jsonObject.optString("isProviderReviewed").equalsIgnoreCase("0")) {
                            showReviewPending(jsonObject, context);

                        }
                    } else if (jsonObject.optString("status").equalsIgnoreCase("Waitingforpaymentconfirmation")) {
                        showPaymentConfirmation(jsonObject.optString("id"), context);
                    } else if (jsonObject.optString("status").equalsIgnoreCase("Finished")) {
                        if (jsonObject.optString("isProviderReviewed").equalsIgnoreCase("0")) {
                            showReviewPending(jsonObject, context);

                        }
                    } else if (jsonObject.optString("status").equalsIgnoreCase("Blocked")) {
                        showBlocked(context);
                    } else if (jsonObject.optString("status").equalsIgnoreCase("Dispute")) {
                        showDispute(context);
                    }


                }

            }
        });
    }

    public static void showReviewPending(final JSONObject jsonObject, final Context context) {

        try {

            dialog.dismiss();
        } catch (Exception e) {

        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        bottomSheetView =inflater.inflate(R.layout.view_rating, null);
//        bottomSheetDialog = new BottomSheetDialog(context);
//        bottomSheetDialog.setContentView(bottomSheetView);
//        bottomSheetBehavior = BottomSheetBehavior.from((View) bottomSheetView.getParent());
//        bottomSheetDialog.show();
//        bottomSheetDialog.setCancelable(false);
//        bottomSheetDialog.setCanceledOnTouchOutside(false);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//
//        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//            }
//        });

        Utils.log(TAG, ":reviewCheck " + jsonObject);
        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_rating);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        Button submit = (Button) dialog.findViewById(R.id.submit);
        Utils.setCustomButton(context, submit);
        final LinearLayout topLayout = (LinearLayout) dialog.findViewById(R.id.topLayout);
        final EditText feedBackText = (EditText) dialog.findViewById(R.id.feedBackText);
        final LinearLayout commentSection = (LinearLayout) dialog.findViewById(R.id.commentSection);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (!isCommentVisible) {
                    animateUp(commentSection, context, topLayout);
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    postValues(context, ratingBar.getRating(), jsonObject.optString("id"), feedBackText.getText().toString(), jsonObject.optString("user_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                commentSection.setVisibility(View.GONE);
//                isCommentVisible=false;
            }
        });


    }

    public static void showBlocked(Context context) {
//        dialog = getLayoutInflater().inflate(R.layout.view_blocked, null);
//        dialog = new dialog(MainActivity.this);
//        dialog.setContentView(dialog);
//        bottomSheetBehavior = BottomSheetBehavior.from((View) dialog.getParent());
//        dialog.show();
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//
//        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//            }
//        });

        try {

            dialog.dismiss();
        } catch (Exception e) {

        }

        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_blocked);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();
    }

    public static void showDispute(Context context) {
//        dialog = getLayoutInflater().inflate(R.layout.view_dispute, null);
//        dialog = new dialog(MainActivity.this);
//        dialog.setContentView(dialog);
//        bottomSheetBehavior = BottomSheetBehavior.from((View) dialog.getParent());
//        dialog.show();
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//
//        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
//                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//            }
//        });
        try {

            dialog.dismiss();
        } catch (Exception e) {

        }


        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_dispute);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();
    }

    public static void postValues(final Context context, float rating, String booking_id, String s, String provider_id) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        int f_rating = Math.round(rating);
        jsonObject.put("booking_id", booking_id);
        jsonObject.put("rating", "" + f_rating);
        jsonObject.put("id", provider_id);
        jsonObject.put("feedback", s);
        ApiCall.PostMethodHeaders(context, UrlHelper.REVIEW, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                isCommentVisible = false;
                dialog.dismiss();
                showThanks(context);
            }
        });
    }

    public static void showThanks(final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {

            dialog.dismiss();
        } catch (Exception e) {

        }


        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.show_thanks);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();


        final CircleImageView confirmed = (CircleImageView) dialog.findViewById(R.id.paymentConfirmed);
        final ImageView paymentWaiting = (ImageView) dialog.findViewById(R.id.paymentWaiting);
        final TextView payConfText = (TextView) dialog.findViewById(R.id.payConfText);
        paymentWaiting.setVisibility(View.VISIBLE);
        Utils.setIconColour(context, paymentWaiting);
        confirmed.setVisibility(View.GONE);
        payConfText.setText(context.getResources().getString(R.string.thanks_for));
        Button confirm = (Button) dialog.findViewById(R.id.confirm);
        Utils.setCustomButton(context, confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPendingRequests(context);
                dialog.dismiss();
            }
        });

    }

    private static void animateUp(LinearLayout commentSection, Context context, final LinearLayout topLayout) {
        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) topLayout.getLayoutParams();
        params.bottomMargin = 55;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slidefrombottomm);
        isCommentVisible = true;
        commentSection.setVisibility(View.VISIBLE);
        commentSection.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slidefrombottom));

        topLayout.setAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                topLayout.setLayoutParams(params);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }

    private static void showPaymentConfirmation(final String id, final Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {

            dialog.dismiss();
        } catch (Exception e) {

        }


        dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.view_payment_confirmation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.BOTTOM);
        dialog.show();


        final Button submit = (Button) dialog.findViewById(R.id.submit);
        Utils.setCustomButton(context, submit);
        CircleImageView circleImageView = dialog.findViewById(R.id.payConfImage);
        Utils.setCircleImageView(context, circleImageView);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    hitPaymentConfirmation(id, context);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void hitPaymentConfirmation(String id, final Context context) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        ApiCall.PostMethodHeaders(context, UrlHelper.PAYMENT_CONFIRMATION, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                getPendingRequests(context);

            }
        });
    }

    private static void showRequestDialog(final JSONObject response, final Context context) {
        try {

            dialog.dismiss();
        } catch (Exception e) {

        }

        final Dialog dialog = new Dialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.request_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();


        TextView userName, descriptionText;
        ImageView userImage;
        LinearLayout acceptButton, rejectButton;
        acceptButton = (LinearLayout) dialog.findViewById(R.id.acceptButton);
        rejectButton = (LinearLayout) dialog.findViewById(R.id.rejectButton);
        userImage = (ImageView) dialog.findViewById(R.id.userImage);
        Utils.setProfilePicture(context, userImage);
        userName = (TextView) dialog.findViewById(R.id.userName);
        descriptionText = (TextView) dialog.findViewById(R.id.descriptionText);
        String name = response.optString("Name");
        userName.setText(name);
        Glide.with(context).load(response.optString("image")).placeholder(context.getResources().getDrawable(R.drawable.profile_pic)).into(userImage);
        String[] split_name = name.split(" ");
        String f_name = split_name[0];
        String service_name = response.optString("sub_category_name");
        String text = "<font color=#1d1d1d><b>" + f_name + "</b></font> " + context.getResources().getString(R.string.sent_you_a_request) + " <font color=#1d1d1d> <b>" + service_name + "</b></font>";
        descriptionText.setText(Html.fromHtml(text));
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                acceptJob(response.optString("id"), dialog, context);
            }
        });

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rejectJob(response.optString("id"), dialog, context);
            }
        });

    }

    private static void rejectJob(String id, final Dialog dialog, final Context context) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(context, UrlHelper.REJECT_JOB, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                dialog.dismiss();
                getPendingRequests(context);
            }
        });
    }

    private static void acceptJob(String id, final Dialog dialog, final Context context) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiCall.PostMethodHeaders(context, UrlHelper.ACCEPT_JOB, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                dialog.dismiss();
                getPendingRequests(context);

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Status event) {
        getPendingRequests(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        getPendingRequests(getActivity());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_home, container, false);
        bookingsList = view.findViewById(R.id.bookingsList);
        empty_layout = view.findViewById(R.id.empty_layout);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPendingRequests(getActivity());
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}

