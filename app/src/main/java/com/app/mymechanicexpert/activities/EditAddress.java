package com.app.mymechanicexpert.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.mymechanicexpert.R;
import com.app.mymechanicexpert.Volley.ApiCall;
import com.app.mymechanicexpert.Volley.VolleyCallback;
import com.app.mymechanicexpert.helpers.SharedHelper;
import com.app.mymechanicexpert.helpers.UrlHelper;
import com.app.mymechanicexpert.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class EditAddress extends AppCompatActivity implements View.OnClickListener {
    EditText addressOne, addressTwo, city, state, zipCode;
    Button bottomButton;
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(EditAddress.this, "theme_value");
        Utils.SetTheme(EditAddress.this, theme_value);
        setContentView(R.layout.activity_edit_address);
        initViews();
        try {
            setValues();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        initListeners();
    }

    private void initListeners() {
        bottomButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    private void setValues() throws JSONException {
        JSONObject generalObject = new JSONObject(getIntent().getStringExtra("provider_details"));

        if (generalObject.isNull("addressline1")) {
            addressOne.setText("");

        } else {
            addressOne.setText(generalObject.optString("addressline1"));

        }


        if (generalObject.isNull("addressline2")) {
            addressTwo.setText("");

        } else {
            addressTwo.setText(generalObject.optString("addressline2"));

        }


        if (generalObject.isNull("city")) {
            city.setText("");

        } else {
            city.setText(generalObject.optString("city"));

        }


        if (generalObject.isNull("state")) {
            state.setText("");

        } else {
            state.setText(generalObject.optString("state"));

        }

        if (generalObject.isNull("zipcode")) {
            zipCode.setText("");

        } else {
            zipCode.setText(generalObject.optString("zipcode"));

        }


    }

    private void initViews() {
        addressOne = (EditText) findViewById(R.id.addressOne);
        addressTwo = (EditText) findViewById(R.id.addressTwo);
        city = (EditText) findViewById(R.id.city);
        state = (EditText) findViewById(R.id.state);
        zipCode = (EditText) findViewById(R.id.zipCode);
        bottomButton = (Button) findViewById(R.id.bottomButton);
        Utils.setCustomButton(EditAddress.this, bottomButton);
        backButton = (ImageView) findViewById(R.id.backButton);


    }

    @Override
    public void onClick(View view) {
        if (view == bottomButton) {
            try {
                registerValues();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (view == backButton) {
            finish();
        }
    }

    private void registerValues() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("addressline1", addressOne.getText().toString());
        jsonObject.put("addressline2", addressTwo.getText().toString());
        jsonObject.put("city", city.getText().toString());
        jsonObject.put("state", state.getText().toString());
        jsonObject.put("zipcode", zipCode.getText().toString());
        ApiCall.PostMethodHeaders(EditAddress.this, UrlHelper.UPDATE_ADDRESS, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(EditAddress.this, getResources().getString(R.string.address_updated_sucessfully));
                moveMainActivity();

            }
        });
    }

    private void moveMainActivity() {
        Intent intent = new Intent(EditAddress.this, MainActivity.class);
        startActivity(intent);
    }
}
