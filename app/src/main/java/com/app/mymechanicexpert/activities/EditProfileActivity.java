package com.app.mymechanicexpert.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.app.mymechanicexpert.R;
import com.app.mymechanicexpert.Volley.ApiCall;
import com.app.mymechanicexpert.Volley.VolleyCallback;
import com.app.mymechanicexpert.helpers.AppSettings;
import com.app.mymechanicexpert.helpers.SharedHelper;
import com.app.mymechanicexpert.helpers.UrlHelper;
import com.app.mymechanicexpert.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Random;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "tag";
    EditText firstName, lastName, mobileNumber, dateOfBirth, aboutYou;
    Spinner genderSpinner;
    Button bottomButton;
    ImageView profileImage;
    String[] genders;
    ImageView backButton;
    private Uri uri;
    private File uploadFile;
    private String uploadImagepath = " ";
    private AppSettings appSettings;
    private final int cameraIntent = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(EditProfileActivity.this, "theme_value");
        Utils.SetTheme(EditProfileActivity.this, theme_value);
        setContentView(R.layout.activity_edit_profile);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        genders = new String[]{getResources().getString(R.string.male), getResources().getString(R.string.female), getResources().getString(R.string.other)};
        initViews();
        initAdapters();
        initListners();
        try {
            setValues();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initListners() {
        bottomButton.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        backButton.setOnClickListener(this);
    }

    private void setValues() throws JSONException {
        JSONObject generalObject = new JSONObject(getIntent().getStringExtra("provider_details"));
        firstName.setText(generalObject.optString("first_name"));
        lastName.setText(generalObject.optString("last_name"));
        mobileNumber.setText(generalObject.optString("mobile"));
        dateOfBirth.setText(generalObject.optString("dob"));
        aboutYou.setText(generalObject.optString("about"));
        if (generalObject.optString("gender").equalsIgnoreCase("M")) {
            genderSpinner.setSelection(0);
        } else {
            genderSpinner.setSelection(1);
        }
        uploadImagepath = generalObject.optString("image");
        Glide.with(this).load(uploadImagepath).placeholder(getResources().getDrawable(R.drawable.profile_pic)).dontAnimate().into(profileImage);

    }

    private void initAdapters() {
        ArrayAdapter aa = new ArrayAdapter(this, R.layout.gender_items, genders);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(aa);
    }

    private void initViews() {
        appSettings = new AppSettings(EditProfileActivity.this);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        dateOfBirth = (EditText) findViewById(R.id.dateOfBirth);
        dateOfBirth.setEnabled(false);
        aboutYou = (EditText) findViewById(R.id.aboutYou);
        genderSpinner = (Spinner) findViewById(R.id.genderSpinner);
        profileImage = (ImageView) findViewById(R.id.profileImage);
        Utils.setProfilePicture(EditProfileActivity.this, profileImage);
        bottomButton = (Button) findViewById(R.id.bottomButton);
        Utils.setCustomButton(EditProfileActivity.this, bottomButton);
        backButton = (ImageView) findViewById(R.id.backButton);
    }


    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);

    }

    private void takePhotoFromCamera() {

        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(),
                getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder,
                getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }

        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

//        uri = Uri.fromFile(file);

//        uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(EditProfileActivity.this, getPackageName() + ".provider", file);
            Log.d(TAG, "onClick: " + uri.getPath());
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        } else {
            uri = Uri.fromFile(file);
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        }
        Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());

        /*Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, 2);*/
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.storage_permission_error));

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1://gallery
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {
                        handleimage(uri);
                    } else {
                        Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to_select));
                    }
                }
                break;
            case cameraIntent://camera
                uploadFile = new File(String.valueOf(appSettings.getImageUploadPath()));
                if (uploadFile.exists()) {
                    Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        profileImage.setColorFilter(ContextCompat.getColor(EditProfileActivity.this, R.color.transparent));
                        Glide.with(EditProfileActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .into(profileImage);
                    } else {
                        profileImage.setColorFilter(getResources().getColor(R.color.transparent));
                        Glide.with(EditProfileActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .into(profileImage);
                    }
                }
                /*if (data != null) {
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    profileImage.setImageBitmap(imageBitmap);
                    Uri tempUri = Utils.getImageUri(getApplicationContext(), imageBitmap);
                    uploadFile = new File(Utils.getRealPathFromURI(EditProfileActivity.this, tempUri));
                }*/
                break;


        }
    }

    private void handleimage(Uri uri) {

        profileImage.setColorFilter(getResources().getColor(R.color.transparent));
        Glide.with(EditProfileActivity.this)
                .load(Utils.getRealPathFromUriNew(EditProfileActivity.this, uri))
                .into(profileImage);
        uploadFile = new File(Utils.getRealPathFromURI(EditProfileActivity.this, uri));

    }


    @Override
    public void onClick(View view) {
        if (view == profileImage) {
            showPictureDialog();
        } else if (view == bottomButton) {
            if (uploadFile == null) {
                try {
                    registerValues();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "uploadFile: " + uploadFile);
                uploadImage();
            }
        } else if (view == backButton) {
            finish();
        }
    }

    private void uploadImage() {
        ApiCall.uploadImage(uploadFile, EditProfileActivity.this, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                uploadImagepath = response.optString("image");
                Log.e(TAG, "uploadFile2: " + uploadImagepath);
                try {
                    registerValues();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void registerValues() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("first_name", firstName.getText().toString());
        jsonObject.put("last_name", lastName.getText().toString());
        jsonObject.put("dob", dateOfBirth.getText().toString());
        jsonObject.put("mobile", mobileNumber.getText().toString());
        jsonObject.put("image", uploadImagepath);
        jsonObject.put("about", aboutYou.getText().toString());
        if (genderSpinner.getSelectedItemPosition() == 0) {
            jsonObject.put("gender", "M");
        } else {
            jsonObject.put("gender", "F");
        }
        ApiCall.PostMethodHeaders(EditProfileActivity.this, UrlHelper.UPDATE_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Utils.toast(EditProfileActivity.this, getResources().getString(R.string.profile_updated_successfully));
                moveMainActivity();
            }
        });


    }

    private void moveMainActivity() {
        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
        intent.putExtra("type", "false");
        startActivity(intent);
    }
}
