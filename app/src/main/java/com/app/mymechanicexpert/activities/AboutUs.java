package com.app.mymechanicexpert.activities;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.app.mymechanicexpert.R;
import com.app.mymechanicexpert.Volley.ApiCall;
import com.app.mymechanicexpert.Volley.VolleyCallback;
import com.app.mymechanicexpert.helpers.UrlHelper;

import org.json.JSONArray;
import org.json.JSONObject;


public class AboutUs extends AppCompatActivity {
    WebView webView;
    ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        webView = (WebView) findViewById(R.id.webView);
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ApiCall.getMethod(AboutUs.this, UrlHelper.ABOUT_US, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                JSONArray static_pages = response.optJSONArray("static_pages");
                JSONObject jsonObject = new JSONObject();

                String url = jsonObject.optString("page_url");
                webView.getSettings().setJavaScriptEnabled(false);
                webView.loadUrl(url);
                webView.setHorizontalScrollBarEnabled(false);
            }
        });


    }
}
